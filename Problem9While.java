import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input n: ");
        int row = kb.nextInt();
        int nn = 1;
        while(nn<=row){
            int column = 1 ;
            while(column<=row){
                System.out.print(column);
                column++;
            }
            System.out.println();
            nn++;
        }
    }
}
