import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            int select = kb.nextInt();
            if(select == 1){
                System.out.print("Please input number: ");
                int num = kb.nextInt();
                for(int i = 0; i<num; i++){
                    for(int j = 0; j<=i; j++){
                        System.out.print('*');
                    }
                    System.out.println();
                }
            
            }else if (select == 2){
                System.out.print("Please input number: ");
                int num = kb.nextInt();
                for(int i = num; i>0; i--){
                    for(int j = 0; j<i; j++){
                        System.out.print('*');
                    }
                    System.out.println();
                }
            }else if(select == 3){
                System.out.print("Please input number: ");
                int num = kb.nextInt();
                for (int i = 0; i < num; i++) {
                    for (int j = 0; j < num; j++) {
                        if (j >= i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }

            }else if (select == 4){
                System.out.print("Please input number: ");
                int num = kb.nextInt();
                for(int i = num; i>0; i--){
                    for(int j = 0; j<=num; j++){
                        if (j>=i){
                            System.out.print("*");
                        }else{
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }else if (select == 5){
                System.out.print("Bye bye!!!");
                break;
            }else{
                System.out.println("Error: Please input number between 1-5");
            }
        }
        kb.close();
    }
}
